# Board project #

This README describe a requirements of implementation Black board and describe all what need to run.

#### Submit a code ####
Create a fork of repository before start woriking
after push all commints to your repository please create a pull request on parent repository (use different branch thant master).

#### How do I get set up? ####

* For build application you need only [Maven](https://maven.apache.org/download.cgi) link to download is [here](http://apache.miloslavbrada.cz/maven/maven-3/3.5.4/binaries/apache-maven-3.5.4-bin.zip) 
    * unpack it to some folder in your computer and add path to maven_dir/bin to system class path
    * after checkout project from repository you can try build it from command line (go to project folder and run command ``mvn build``)

## Project Tasks ##

### Base Requirement ###

#### Implement Shape ####
* CRUD operation with shape (CRUD = Create, Read, Update, Delete)
* Implement these types of shapes
    * Square (exists)
    * Circle
    * Triangle
    * Rectangle
* Every shape will have
    * Name
    * Color
    * Distance
    * Unique Identifier (It could be Long or [UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier))
* On Every shape should have
    * calculate area and volume
    * print detail of itself
    
#### Implement Board ####
* Define board distance and Shape
* CRUD operation with shape on Board (use methods from CRUD on Shape)
* check if new shape have enough space to add on board 

### Another task to implementation ###

#### Extends shape functionality ####
* add coordination of shape on board
* add ability to rotate the shape on board

#### Extends Board Functionality ###

* print all shapes by type of shape or color
* print all shapes sorted by 
    * distance
    * color
    * type
* check the identical shape in board (by distance, type, color) and ask user to add or not.
